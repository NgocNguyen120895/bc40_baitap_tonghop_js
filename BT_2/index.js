numberArr = [-99, -5, -2, -1, 0, 1, 2, 5, 22, 88, 100, 201, 2929, 999, 3, 801, 113, 71, 997, 307];

function kiem_tra_snt(n) {
    var flag = true;
    // Nếu n bé hơn 2 tức là không phải số nguyên tố
    if (n < 2) {
        flag = false;
    } else if (n == 2) {
        flag = true;
    } else if (n % 2 == 0) {
        flag = false;
    } else {
        // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
        for (var i = 3; i < n - 1; i += 2) {
            if (n % i == 0) {
                flag = false;
                break;
            }
        }
    }
    // Kiểm tra biến flag
    if (flag == true) {
        document.write(n + " là số nguyên tố <br/>");
    }
    else {
        document.write(n + " không phải là số nguyên tố <br/>");
    }
}

numberArr.forEach((element, index) => {
    var checkValue = numberArr[index];
    kiem_tra_snt(checkValue);
});
