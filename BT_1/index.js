var contentHTML = "";
for (var i = 1; i <= 100; i++) {
    var iString = String(i).padStart(2, "0");
    var content =/*html*/ `
        <span>${iString}</span>
        `
    if (iString % 10 == 0) {
        content = ` <span>${iString}</span> <br>`
    }
    contentHTML += content;
}
document.getElementById('output').innerHTML = contentHTML;
// console.log(contentHTML)