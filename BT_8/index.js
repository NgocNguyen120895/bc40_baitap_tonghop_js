
deckCards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"];

player1 = [], player2 = [], player3 = [], player4 = [];

for (let deal = 0; deal < deckCards.length; deal += 4) {
    player1.push(deckCards[deal]);
    player2.push(deckCards[deal + 1]);
    player3.push(deckCards[deal + 2]);
    player4.push(deckCards[deal + 3]);
}

console.log("player1: ", player1)
console.log("player2: ", player2)
console.log("player3: ", player3)
console.log("player4: ", player4)
