

let reverseString = (value) => {
    let newStr = "";
    for (let i = value.length - 1; i >= 0; i--) {
        newStr += value[i];
    }
    return newStr;
}

let revert = () => {
    let n = document.getElementById('n_input').value;
    document.getElementById('output').innerHTML = reverseString(n);;
}

